# Tenth Theory

This is a test project written with python/django.
Implemented a simple Student Registration System(SRS)

## Getting Started

* Clone the project.
* Install the requirements.
```
pip install -r requirements.txt
```

* You are ready to go...

### *Note
A DockerFile and  docker-compose file exists in the project root;You can use them.

## In case you want to run tests and  get a report

Run this commands:
```
coverage erase
coverage run --branch --parallel-mode manage.py test srs
coverage combine
coverage html
```
A htmlcov directory will be created in project root.
Open the directory and find index.html file.
Open this file in browser to see the testing reports

## Authors

* **Name: Parsa Eini** 
* **Email: parsaeini1998@gmail.com** 
