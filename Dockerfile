FROM python:3.8
WORKDIR /code
RUN pip install --upgrade pip
ADD requirements.txt /code/
RUN pip install -r requirements.txt
RUN pip install gunicorn
ADD . /code/
CMD python manage.py migrate && python manage.py runserver 0.0.0.0:8001