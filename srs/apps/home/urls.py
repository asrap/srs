from django.urls import path

from srs.apps.home.views import HomeView

app_name = "students"

urlpatterns = [
    path('', HomeView.as_view(), name='index'),
]
