from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView

from srs.apps.students.forms import StudentForm, StudentSearchForm
from srs.apps.students.models import Student
from srs.apps.students.services.student import StudentService


class StudentDetailView(DetailView):
    template_name = "students/student_detail.html"
    pk_url_kwarg = "id"
    context_object_name = "student"

    def get_context_data(self, **kwargs):
        context = super(StudentDetailView, self).get_context_data(**kwargs)
        student_kwargs = StudentService.get_student_search_kwargs(self.request.GET)
        context.update({
            "same_grade_students": StudentService.get_students(**student_kwargs),
            "form": StudentSearchForm()
        })
        return context

    def get_queryset(self):
        return StudentService.get_same_grade_students_by_id(self.kwargs.get("id"))


@method_decorator(login_required, name="get")
@method_decorator(login_required, name="post")
class StudentCreateView(CreateView):
    form_class = StudentForm
    template_name = "students/student.html"

    def get(self, request, *args, **kwargs):
        if StudentService.student_exists(user=self.request.user):
            student = StudentService.get_student_by_user(self.request.user)
            return redirect(reverse("students:edit", kwargs={"id": student.id}))
        return super(StudentCreateView, self).get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(StudentCreateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse("students:detail", kwargs={"id": self.object.pk})


@method_decorator(login_required, name="get")
@method_decorator(login_required, name="post")
class StudentUpdateView(UpdateView):
    form_class = StudentForm
    model = Student
    pk_url_kwarg = "id"
    template_name = "students/student.html"
    extra_context = {"update": True}

    def get_form_kwargs(self):
        kwargs = super(StudentUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse("students:detail", kwargs={"id": self.object.id})

    def get_queryset(self):
        if StudentService.student_exists(id=self.kwargs.get("id"), user=self.request.user):
            return super(StudentUpdateView, self).get_queryset()
        raise Http404
