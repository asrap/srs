from django.test import TestCase, Client
from django.urls import reverse

from srs.apps.authentication.services.auth import UserService
from srs.apps.students.forms import StudentSearchForm, StudentForm
from srs.apps.students.services.student import StudentService


#
class StudentDetailViewTestCase(TestCase):
    fixtures = ["auth", "students"]

    def setUp(self):
        user = UserService.get_user_by_id(1)
        user.set_password("123456")
        user.save()
        self.client = Client()
        self.client.login(username="user1", password="123456")

    def test_get(self):
        url = reverse("students:detail", kwargs={"id": 1})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.template_name), 1)
        self.assertEqual(response.template_name[0], "students/student_detail.html")

        self.assertIsInstance(response.context_data['form'], StudentSearchForm)


class StudentCreateViewTestCase(TestCase):
    fixtures = ["auth", ]

    def setUp(self):
        self.user = UserService.get_user_by_id(1)
        self.user.set_password("123456")
        self.user.save()
        self.client = Client()
        self.client.login(username="user1", password="123456")

    def test_get(self):
        url = reverse("students:register")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_get_user_has_student(self):
        StudentService.create_student(
            user=self.user,
            first_name="first_name",
            last_name="last_name",
            father_name="father_name",
            national_id="1122334455",
            telephone="11223344",
            grade="1"
        )

        url = reverse("students:register")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 302)

    def test_post(self):
        data = {
            "user": 1,
            "first_name": "first_name",
            "last_name": "last_name",
            "father_name": "father_name",
            "national_id": "1122334455",
            "telephone": "11223344",
            "grade": "1"
        }
        url = reverse("students:register")
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)


class StudentUpdateViewTestCase(TestCase):
    fixtures = ["auth", ]

    def setUp(self):
        self.user = UserService.get_user_by_id(1)
        self.user.set_password("123456")
        self.user.save()
        StudentService.create_student(
            user=self.user,
            first_name="first_name",
            last_name="last_name",
            father_name="father_name",
            national_id="1122334455",
            telephone="11223344",
            grade="1"
        )
        self.client = Client()
        self.client.login(username="user1", password="123456")

    def test_get(self):
        url = reverse("students:edit", kwargs={"id": 1})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context_data['form'], StudentForm)

    def test_post(self):
        data = {
            "user": 1,
            "first_name": "first_name",
            "last_name": "last_name",
            "father_name": "father_name",
            "national_id": "1122334455",
            "telephone": "11223344",
            "grade": "2"
        }
        url = reverse("students:edit", kwargs={"id": 1})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_post_404(self):
        url = reverse("students:edit", kwargs={"id": 2})
        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 404)
