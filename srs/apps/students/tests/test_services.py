from django.test import TestCase

from srs.apps.authentication.services.auth import UserService
from srs.apps.students.services.student import StudentService


class StudentServiceTestCase(TestCase):
    fixtures = ["auth", "students"]

    def test_get_student_not_exist(self):
        self.assertIsNone(StudentService.get_student_by_id(1000))

    def test_get_student_by_id(self):
        student = StudentService.get_student_by_id(1)

        self.assertEqual(student.grade, '1')
        self.assertEqual(student.first_name, "student1_firstname")
        self.assertEqual(student.last_name, "student1_lastname")
        self.assertEqual(student.father_name, "student1_fathername")
        self.assertEqual(student.user.id, 1)

    def test_get_student_by_user(self):
        expected_student = StudentService.get_student_by_id(1)

        student = StudentService.get_student_by_user(UserService.get_user_by_id(1))

        self.assertEqual(student.user.id, expected_student.user.id)
        self.assertEqual(student.first_name, expected_student.first_name)
        self.assertEqual(student.last_name, expected_student.last_name)
        self.assertEqual(student.father_name, expected_student.father_name)
        self.assertEqual(student.grade, expected_student.grade)

    def test_get_all_students(self):
        all_students = StudentService.get_all_students()

        self.assertEqual(len(all_students), 1)

    def test_get_same_grade_students_by_id(self):
        students = StudentService.get_same_grade_students_by_id(1)

        self.assertEqual(len(students), 1)
        self.assertEqual(students[0].grade, "1")

    def test_student_exists(self):
        self.assertTrue(StudentService.student_exists(first_name="student1_firstname"))

    def test_student_does_not_exist(self):
        self.assertFalse(StudentService.student_exists(id=1000))

    def test_get_student_search_kwargs(self):
        expected = {
            "first_name": "first_name",
            "last_name": "last_name",
            "father_name": "father_name"
        }

        kwargs = StudentService.get_student_search_kwargs(expected)

        self.assertDictEqual(kwargs, expected)

    def test_get_student_search_kwargs_no_first_name(self):
        expected = {
            "last_name": "last_name",
            "father_name": "father_name"
        }

        kwargs = StudentService.get_student_search_kwargs(expected)

        self.assertDictEqual(kwargs, expected)

    def test_get_student_search_kwargs_no_last_name(self):
        expected = {
            "first_name": "first_name",
            "father_name": "father_name"
        }

        kwargs = StudentService.get_student_search_kwargs(expected)

        self.assertDictEqual(kwargs, expected)

    def test_get_student_search_kwargs_no_father_name(self):
        expected = {
            "first_name": "first_name",
            "last_name": "last_name",
        }

        kwargs = StudentService.get_student_search_kwargs(expected)

        self.assertDictEqual(kwargs, expected)

    def test_get_students(self):
        students = StudentService.get_students(first_name="student1_firstname")

        self.assertEqual(len(students), 1)

        self.assertEqual(students[0].first_name, "student1_firstname")
