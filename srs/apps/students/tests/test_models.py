from django.contrib.auth import get_user_model
from django.test import TestCase

from srs.apps.students.services.student import StudentService

User_Model = get_user_model()


class StudentTestCase(TestCase):
    def test_str(self):
        user = User_Model.objects.create(username="test")
        student = StudentService.create_student(
            user=user,
            first_name="first_name",
            last_name="last_name",
            father_name="father_name",
            national_id="1234567890",
            telephone="11223344",
            grade="1"
        )
        self.assertEqual(str(student), "%s %s" % (student.first_name, student.last_name))
