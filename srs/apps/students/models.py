from django.conf import settings
from django.core.validators import MinLengthValidator
from django.db import models


class Student(models.Model):

    FIRST_GRADE = "1"
    SECOND_GRADE = "2"
    THIRD_GRADE = "3"
    FOURTH_GRADE = "4"
    FIFTH_GRADE = "5"
    SIXTH_GRADE = "6"
    SEVENTH_GRADE = "7"
    EIGHTH_GRADE = "8"
    NINTH_GRADE = "9"
    TENTH_GRADE = "10"
    ELEVENTH_GRADE = "11"
    TWELFTH_GRADE = "12"
    GRADE_CHOICES =(
        (FIRST_GRADE, "FIRST_GRADE"),
        (SECOND_GRADE, "SECOND_GRADE"),
        (THIRD_GRADE, "THIRD_GRADE"),
        (FOURTH_GRADE, "FOURTH_GRADE"),
        (FIFTH_GRADE, "FIFTH_GRADE"),
        (SIXTH_GRADE, "SEVENTH_GRADE"),
        (SEVENTH_GRADE, "SEVENTH_GRADE"),
        (EIGHTH_GRADE, "EIGHTH_GRADE"),
        (NINTH_GRADE, "NINTH_GRADE"),
        (TENTH_GRADE, "TENTH_GRADE"),
        (ELEVENTH_GRADE, "ELEVENTH_GRADE"),
        (TWELFTH_GRADE, "TWELFTH_GRADE"),
    )

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name="User", related_name="student"
    )
    first_name = models.CharField("Name", max_length=30)
    last_name = models.CharField("Last Name", max_length=30)
    father_name = models.CharField("Father's Name", max_length=30)
    national_id = models.CharField("National ID", max_length=10, validators=[MinLengthValidator(10)], unique=True)
    telephone = models.CharField("Telephone", max_length=8, validators=[MinLengthValidator(8)])
    image = models.ImageField("Image", upload_to="students/", blank=True, null=True)
    grade = models.CharField("Grade", choices=GRADE_CHOICES, max_length=2)

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Students"

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)
