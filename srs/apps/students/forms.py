from django import forms

from srs.apps.students.models import Student


class StudentForm(forms.ModelForm):
    def __init__(self, *args, user=None, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        self.instance.user = user
        self.fields['first_name'].label = "نام"
        self.fields['first_name'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['last_name'].label = "نام خانوادگی"
        self.fields['last_name'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['father_name'].label = "نام پدر"
        self.fields['father_name'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['national_id'].label = "کد ملی"
        self.fields['national_id'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['telephone'].label = "تلفن محل سکونت"
        self.fields['telephone'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['image'].label = "عکس پرسنلی"
        self.fields['image'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['grade'].label = "مقطع"
        self.fields['grade'].widget.attrs['class'] = "mdl-textfield__input"

    class Meta:
        model = Student
        fields = ("first_name", "last_name", "father_name", "national_id", "telephone", "image", "grade")


class StudentSearchForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(StudentSearchForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = "نام"
        self.fields['first_name'].required = False
        self.fields['first_name'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['last_name'].label = "نام خانوادگی"
        self.fields['last_name'].required = False
        self.fields['last_name'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['father_name'].label = "نام پدر"
        self.fields['father_name'].required = False
        self.fields['father_name'].widget.attrs['class'] = "mdl-textfield__input"

    class Meta:
        model = Student
        fields = ("first_name", "last_name", "father_name")
