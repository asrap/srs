from srs.apps.students.models import Student


class StudentService(object):

    @staticmethod
    def create_student(user, first_name, last_name, father_name, national_id, telephone, grade, image=None):
        return Student.objects.create(
            user=user,
            first_name=first_name,
            last_name=last_name,
            father_name=father_name,
            national_id=national_id,
            telephone=telephone,
            grade=grade,
            image=image
        )

    @staticmethod
    def get_student(*args, **kwargs):
        try:
            return Student.objects.get(*args, **kwargs)
        except Student.DoesNotExist:
            return None

    @staticmethod
    def get_student_by_id(id_):
        return StudentService.get_student(id=id_)

    @staticmethod
    def get_student_by_user(user_):
        return StudentService.get_student(user=user_)

    @staticmethod
    def get_all_students():
        return Student.objects.all()

    @staticmethod
    def get_same_grade_students_by_id(id_):
        student = StudentService.get_student_by_id(id_)
        return Student.objects.filter(grade=student.grade)

    @staticmethod
    def student_exists(**kwargs):
        return Student.objects.filter(**kwargs).exists()

    @staticmethod
    def get_student_search_kwargs(kwargs):
        dic = {}
        if kwargs.get("first_name"):
            dic.update({"first_name": kwargs["first_name"]})

        if kwargs.get("last_name"):
            dic.update({"last_name": kwargs["last_name"]})
        if kwargs.get("father_name"):
            dic.update({"father_name": kwargs["father_name"]})
        return dic

    @staticmethod
    def get_students(**kwargs):
        return Student.objects.filter(**kwargs)
