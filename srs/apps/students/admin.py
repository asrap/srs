from django.contrib import admin

from srs.apps.students.models import Student

admin.site.register(Student)
