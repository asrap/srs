from django.urls import path

from srs.apps.students.views import StudentCreateView, StudentDetailView, StudentUpdateView

app_name = "students"

urlpatterns = [
    path('register/', StudentCreateView.as_view(), name='register'),
    path('<int:id>/', StudentDetailView.as_view(), name='detail'),
    path('<int:id>/edit/', StudentUpdateView.as_view(), name='edit'),
]
