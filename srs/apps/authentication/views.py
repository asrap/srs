from django.contrib.auth import login
from django.contrib.auth.views import LoginView as UserLoginView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from srs.apps.authentication.forms import SignUpForm, LoginForm
from srs.apps.students.services.student import StudentService


class SignUpView(CreateView):
    form_class = SignUpForm
    template_name = "authentication/auth.html"

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("students:register")


class LoginView(UserLoginView):
    template_name = "authentication/auth.html"
    extra_context = {"login": True}
    form_class = LoginForm

    def get_success_url(self):
        student = StudentService.get_student_by_user(self.request.user)
        if student:
            return reverse("students:edit", kwargs={"id": student.id})
        return super(LoginView, self).get_success_url()
