from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "نام کاربری"
        self.fields['email'].label = "ایمیل"
        self.fields['password1'].label = "رمز عبور"
        self.fields['password1'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['password2'].label = "تایید رمز عبور"
        self.fields['password2'].widget.attrs['class'] = "mdl-textfield__input"

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
        widgets = {
            'username': forms.TextInput(attrs={'class': "mdl-textfield__input"}),
            'email': forms.TextInput(attrs={'class': "mdl-textfield__input", }),
        }


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "نام کاربری"
        self.fields['username'].widget.attrs['class'] = "mdl-textfield__input"
        self.fields['password'].label = "رمز عبور"
        self.fields['password'].widget.attrs['class'] = "mdl-textfield__input"
