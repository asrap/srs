from django.test import TestCase

from srs.apps.authentication.services.auth import UserService


class UserServiceTestCase(TestCase):
    fixtures = ["auth"]

    def test_get_user_by_id(self):
        user = UserService.get_user_by_id(1)

        self.assertEqual(user.first_name, "user1_firstname")
        self.assertEqual(user.last_name, "user1_lastname")
        self.assertEqual(user.email, "user1@test.com")

    def test_get_user_by_id_none(self):
        self.assertIsNone(UserService.get_user_by_id(1000))
