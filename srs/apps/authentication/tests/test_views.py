from django.test import TestCase, Client
from django.urls import reverse

from srs.apps.authentication.forms import SignUpForm, LoginForm
from srs.apps.authentication.services.auth import UserService
from srs.apps.students.services.student import StudentService


class SignUpViewTestCase(TestCase):
    def test_get(self):
        url = reverse("authentication:register")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context_data['form'], SignUpForm)
        self.assertFalse(response.context_data['form'].is_bound)

    def test_post(self):
        data = {
            "username": "test-username",
            "password1": "12345678i",
            "password2": "12345678i"
        }
        url = reverse("authentication:register")
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)


class LoginViewTestCase(TestCase):
    fixtures = ['auth']

    def test_get(self):
        url = reverse("authentication:login")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context_data['form'], LoginForm)
        self.assertFalse(response.context_data['form'].is_bound)

    def test_post(self):
        data = {
            "username": "user1",
            "password": "123456",
        }
        url = reverse("authentication:login")
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)

    def test_post_student_exists(self):
        StudentService.create_student(
            user=UserService.get_user_by_id(1),
            first_name="first_name",
            last_name="last_name",
            father_name="father_name",
            national_id="1122334455",
            telephone="11223344",
            grade="1"
        )
        data = {
            "username": "user1",
            "password": "123456",
        }
        url = reverse("authentication:login")
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)
