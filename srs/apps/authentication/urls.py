from django.urls import path

from srs.apps.authentication.views import SignUpView, LoginView

app_name = "authentication"

urlpatterns = [
    path('sign-up/', SignUpView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
]
