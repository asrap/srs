from django.contrib.auth import get_user_model

UserModel = get_user_model()


class UserService(object):

    @staticmethod
    def get_user(*args, **kwargs):
        try:
            return UserModel.objects.get(*args, **kwargs)
        except UserModel.DoesNotExist:
            return None

    @staticmethod
    def get_user_by_id(id_):
        return UserService.get_user(id=id_)
