from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include("srs.apps.home.urls", namespace='home')),
    path('auth/', include("srs.apps.authentication.urls", namespace='authentication')),
    path('students/', include("srs.apps.students.urls", namespace='students')),
    path('admin/', admin.site.urls, ),
]
